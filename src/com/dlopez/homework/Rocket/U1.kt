package com.dlopez.homework.Rocket

import com.dlopez.homework.Rocket.Rocket
import kotlin.random.Random

class U1() : Rocket() {

    override var rocketCost = 100000000
    override var rocketWeight = 10000
    override var maxWeight = 18000
    override var currentWeight = rocketWeight

    override fun launch(): Boolean {
        /**
         * chance of launching fail = 5 % * (cargo carried/cargo limit)
         */
        val randomValue = (Random.nextDouble(from = 0.0, until = 0.2)).toFloat()
        val chanceOfExplosion = 0.05f * (currentWeight.toFloat() / maxWeight.toFloat())
        return chanceOfExplosion <= randomValue
    }

    override fun land(): Boolean {
        /**
         * chance of landing crash = 1% * (cargo carried/cargo limit)
         */
        val randomValue = (Random.nextDouble(from = 0.0, until = 0.2)).toFloat()
        val chanceOfCrash = 0.01f * (currentWeight.toFloat() / maxWeight.toFloat())
        return chanceOfCrash <= randomValue
    }
}



